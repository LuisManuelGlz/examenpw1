from django.urls import path
from . import views

app_name = 'teams'
urlpatterns = [
  path('', views.ListTeams.as_view(), name='list-teams'),
  path('<int:pk>', views.DetailTeam.as_view(), name='detail-team'),
]
from django.shortcuts import render
from django.views import generic
from .models import Team

class ListTeams(generic.ListView):
  template_name = 'teams/list_teams.html'
  model = Team
  context_object_name = 'teams'

class DetailTeam(generic.DeleteView):
  template_name = 'teams/detail_team.html'
  model = Team
  context_object_name = 'team'

from django.db import models
from stadiums.models import Stadium

class Team(models.Model):
  name = models.CharField(max_length=150)
  stadium = models.OneToOneField(Stadium, on_delete=models.CASCADE, null=True)
  location = models.CharField(max_length=250)
  timestamp = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return self.name
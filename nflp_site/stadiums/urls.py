from django.urls import path
from . import views

app_name = 'stadiums'
urlpatterns = [
  path('', views.ListStadiums.as_view(), name='list-stadiums'),
  path('<int:pk>', views.DetailStadium.as_view(), name='detail-stadium'),
]
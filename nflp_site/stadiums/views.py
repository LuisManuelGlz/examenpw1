from django.shortcuts import render
from django.views import generic
from .models import Stadium

class ListStadiums(generic.ListView):
  template_name = 'stadiums/list_stadiums.html'
  model = Stadium
  context_object_name = 'stadiums'

class DetailStadium(generic.DeleteView):
  template_name = 'stadiums/detail_stadium.html'
  model = Stadium
  context_object_name = 'stadium'

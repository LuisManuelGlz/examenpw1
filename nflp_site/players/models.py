from django.db import models
from teams.models import Team

class Player(models.Model):
  first_name = models.CharField(max_length=250)
  last_name = models.CharField(max_length=250)
  team = models.ForeignKey(Team, on_delete=models.CASCADE, default=1)
  position = models.CharField(max_length=100)
  timestamp = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return f'{self.first_name} {self.last_name}'
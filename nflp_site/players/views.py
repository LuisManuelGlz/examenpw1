from django.shortcuts import get_object_or_404
from django.views import generic
from .models import Player

class ListPlayers(generic.ListView):
  template_name = 'players/list_players.html'
  model = Player
  context_object_name = 'players'

  def get_queryset(self):
    query = self.request.GET.get('search', '')
    queryForPos = self.request.GET.get('searchForPosition', '')
    
    if query:
      print('hola')
      object_list = Player.objects.filter(team__name__icontains=query)
    elif queryForPos:
      object_list = Player.objects.filter(position__icontains=queryForPos)
    else:
      object_list = Player.objects.all()
    return object_list

class DetailPlayer(generic.DeleteView):
  template_name = 'players/detail_player.html'
  model = Player
  context_object_name = 'player'

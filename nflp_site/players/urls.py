from django.urls import path
from . import views

app_name = 'players'
urlpatterns = [
  path('', views.ListPlayers.as_view(), name='list-players'),
  path('<int:pk>', views.DetailPlayer.as_view(), name='detail-player'),
]